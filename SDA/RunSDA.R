### This script is to run SDAs #################################################
################################################################################



################################################################################
### set up environment & load packages #########################################

esf_dir<-here::here()
setwd(file.path(esf_dir,'SDA'))

load(file.path('..','Analysis',"IOMatrices.RData"))
load(file.path('..','Analysis',"CO2footpr.RData"))
load(file.path('..','Analysis',"ENfootpr.RData"))
load(file.path('..','Analysis',"EMPfootpr.RData"))
load(file.path('..','Analysis',"VAfootpr.RData"))

library(dplyr)
library(tidyr)
library(ggplot2)
library(gridExtra)

source('SDAfunctions.R')


################################################################################
### SDA CO2 ####################################################################

SDA.CB.CO2<-make_SDA(c,
                     c("00-01","01-02","02-03","03-04","04-05","05-06","06-07",
                       "07-08","08-09","09-10","10-11","11-12","12-13","13-14"),
                     DIRECT.CO2,
                     L,
                     Y,
                     B)

SDA.CB.CO2.cs<-make_sectdisagg_SDA(c,cs,DIRECT.CO2,L,Y,B)


# save
save(SDA.CB.CO2, SDA.CB.CO2.cs, file = "SDACO2.RData")


################################################################################
### EMP ########################################################################

SDA.CB.EMP<-make_SDA(c,
                    c("00-01","01-02","02-03","03-04","04-05","05-06","06-07",
                      "07-08","08-09","09-10","10-11","11-12","12-13","13-14"),
                    DIRECT.EMP,
                    L,
                    Y,
                    B)

# save
save(SDA.CB.EMP, file = "SDAEMP.RData")



################################################################################
### SDA value added ############################################################

SDA.CB.VA<-make_SDA(c,
                     c("00-01","01-02","02-03","03-04","04-05","05-06","06-07",
                       "07-08","08-09","09-10","10-11","11-12","12-13","13-14"),
                     DIRECT.VA,
                     L,
                     Y,
                     B)

# save
save(SDA.CB.VA, file = "SDAVA.RData")



