The datafile "Productivities.csv" contains economic productivities of carbon, calculated from WIOD data. All calculations can be found in this repository.

The data is provided for each year 2000 - 2014, for each sector (56 based on NACE classification), for each country (44). We provide three accounts:
  - cprodv: The value added productivity of carbon. Unit: USD (pyp) / kg CO2
  - cprodw: The wage productivity of carbon. Unit: USD (pyp) / kg CO2
  - cprode: The employment productivity of carbon. Unit: number of persons engaged / t CO2

All three accounts are provided in two scopes. The first scope named "direct" is based on direct sectoral carbon emissions. The second scope named "total"
is based on direct + indirect sectoral carbon emissions. Indirect carbon emissions are those incurred in upstream value chains. Calculated using standard
input-output methodology.



